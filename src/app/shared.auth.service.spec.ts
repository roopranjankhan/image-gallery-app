import { TestBed } from '@angular/core/testing';

import { Shared.AuthService } from './shared.auth.service';

describe('Shared.AuthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Shared.AuthService = TestBed.get(Shared.AuthService);
    expect(service).toBeTruthy();
  });
});
