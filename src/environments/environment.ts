// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDWGNZBj91IGFn76M704ldTKBwG4cw34hY',
    authDomain: 'images-6b7f6.firebaseapp.com',
    databaseURL: 'https://images-6b7f6-default-rtdb.europe-west1.firebasedatabase.app',
    projectId: 'images-6b7f6',
    storageBucket: 'images-6b7f6.appspot.com',
    messagingSenderId: '250007523230',
    appId: '1:250007523230:web:fa63fba7a28f743491108f'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
